$(document).ready(function(){
	var ensino;
	var pesquisa;
	var extensao;
	var min_ensino;
	var min_pesquisa;
	var min_extensao;
	
	var advice = false;
	
	ensino = parseInt($('#horas_ensino').html());
	pesquisa = parseInt($('#horas_pesquisa').html());
	extensao = parseInt($('#horas_extensao').html());
	min_ensino = parseInt($('#min_ensino').html());
	min_pesquisa = parseInt($('#min_pesquisa').html());
	min_extensao = parseInt($('#min_extensao').html());
	
	if(ensino < min_ensino){
		advice = true;
		$('#horas_ensino').css('color','red');
		$('#horas_ensino').css('font-weight','bold');
	}
	if(pesquisa < min_pesquisa){
		advice = true;
		$('#horas_pesquisa').css('color','red');
		$('#horas_pesquisa').css('font-weight','bold');
	}

	if(extensao < min_extensao){
		advice = true;
		$('#horas_extensao').css('color','red');
		$('#horas_extensao').css('font-weight','bold');
	}

	if(advice){
		$("#hours_advice").html("<b>Atenção!!!</b><br>Este aluno ainda não concluiu todas horas necessárias! Não é recomendado mudar o status desta análise para 'concluído'.");
	}

});
