<div class="modalities view">
<h2><?php echo __('Modality'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hours'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['hours']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Modality'), array('action' => 'edit', $modality['Modality']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Modality'), array('action' => 'delete', $modality['Modality']['id']), array(), __('Are you sure you want to delete # %s?', $modality['Modality']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Activities'); ?></h3>
	<?php if (!empty($modality['Activity'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Analysi Id'); ?></th>
		<th><?php echo __('Modality Id'); ?></th>
		<th><?php echo __('Type Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Document'); ?></th>
		<th><?php echo __('Hours'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($modality['Activity'] as $activity): ?>
		<tr>
			<td><?php echo $activity['id']; ?></td>
			<td><?php echo $activity['analysi_id']; ?></td>
			<td><?php echo $activity['modality_id']; ?></td>
			<td><?php echo $activity['type_id']; ?></td>
			<td><?php echo $activity['name']; ?></td>
			<td><?php echo $activity['document']; ?></td>
			<td><?php echo $activity['hours']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activities', 'action' => 'view', $activity['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activities', 'action' => 'edit', $activity['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activities', 'action' => 'delete', $activity['id']), array(), __('Are you sure you want to delete # %s?', $activity['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
