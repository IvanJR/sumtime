<?php echo $this -> Html -> script('validate');  ?>
<div class="activities form">
<?php echo $this->Form->create('Activity', array('type' => 'file', 'novalidate'=>true)); ?>
	<fieldset>
		<legend><?php echo 'Editar Atividade' ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('class'=>'form-control', 'label'=>'Nome '));
		echo $this->Form->input('hours', array('class'=>'form-control bfh-number', 'label'=>'Horas '));
		echo $this->Form->input('analysi_id', array('type' => 'hidden'));
		echo $this->Form->input('modality_id', array('class'=>'form-control', 'style' => 'width:236px;', 'label'=>'Modalidade ', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('type_id', array('class'=>'form-control', 'style' => 'width:236px;', 'label'=>'Tipo de Atividade ', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('document_new', array('class'=>'form-control', 'type' => 'file', 'label' => 'Certificado (.pdf)'));
	?>
	</fieldset>
	<?php echo $this->Form->end(); ?>

	<div class="submit" id="btnSalvar">
		<input type="submit" class="btn btn-info" style="width:75px; height:34px; margin-left:10px;" value="Salvar"/>
	</div>
</div>

<div class="actions">
	<?php if($this->Session->read('Auth.User.level') == 2):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $this->request->data['Analysi']['id']));?>'">Retornar à Minha Análise</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>'">Retornar à Lista de Análises</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 5):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>'">Retornar à Painel de Admin</a></li>
	<?php endif; ?>
</div>

<script>
	$('#btnSalvar').click(function(){
		if (validateActivity()){
			$('#ActivityEditForm').submit();
		}
	});
</script>
