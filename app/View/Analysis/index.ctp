<?php
	echo $this->Html->script('jquery-1.3.2.min');
	echo $this->Html->script('select-all');
	echo $this->Html->script('bootstrap-dropdown');
	echo $this -> Html -> script('bootstrap');
?>
<div class="analysis" style="min-height:200px;">

<div class="btn-group" style="float:right;margin-right:20px; width:110px;">

	<button data-toggle="dropdown" class="btn btn-info btn-small dropdown-toggle">
		<?php
			if ($filterStatus) {
				echo $status[$filterStatus];
			} else {
				echo 'Filtro - Status';
			}
		?>
		<span class="caret"></span>
	</button>

	<ul class="dropdown-menu dropdown-info pull-right" style="margin-right:-15px;">
		<?php if($filter): ?>
			<li>
	   			<?php echo $this->Html->link('Pendente', array('controller'=>'analysis','action'=>'index', $filter, 1))?>
	  		</li>
	  		<li>
	   			<?php echo $this->Html->link('Em Análise', array('controller'=>'analysis','action'=>'index', $filter, 2))?>
	  		</li>
	  		<li>
	   			<?php echo $this->Html->link('Concluído', array('controller'=>'analysis','action'=>'index', $filter , 3))?>
	  		</li>
  		<?php else: ?>
  			<li>
	   			<?php echo $this->Html->link('Pendente', array('controller'=>'analysis','action'=>'index', 0, 1))?>
	  		</li>
	  		<li>
	   			<?php echo $this->Html->link('Em Análise', array('controller'=>'analysis','action'=>'index', 0, 2))?>
	  		</li>
	  		<li>
	   			<?php echo $this->Html->link('Concluído', array('controller'=>'analysis','action'=>'index', 0, 3))?>
	  		</li>
  		<?php endif; ?>	
  		<li class="divider"></li>
  		<?php if($filter): ?>
	  		<li>
	   			<?php echo $this->Html->link('Todos', array('controller'=>'analysis','action'=>'index', $filter))?>
	  		</li>
  		<?php else: ?>
  			<li>
	   			<?php echo $this->Html->link('Todos', array('controller'=>'analysis','action'=>'index', $filter))?>
	  		</li>
  		<?php endif; ?>
 	</ul>
</div>
<div class="btn-group" style="float:right;margin-right:110px; width:110px;max-width:110px;">
	<button data-toggle="dropdown" class="btn btn-info btn-small dropdown-toggle" style="width:210px;">
		<?php
			if ($filter) {
				echo $courses[$filter];
			} else {
				echo 'Filtro - Cursos';
			}
		?>
		<span class="caret"></span>
	</button>

	<ul class="dropdown-menu dropdown-info pull-left">
		<?php	foreach ($courses as $key => $obj) {
				   echo '<li>';
				   echo $this->Html->link($obj, array('controller' => 'analysis', 'action' => 'index', $key));
				   echo '</li>';
				}
		?>
  		<li class="divider"></li>
  		<?php if($filterStatus): ?>
	  		<li>
	   			<?php echo $this->Html->link('Todos', array('controller'=>'analysis','action'=>'index', 0, $filterStatus))?>
	  		</li>
  		<?php else: ?>
  			<li>
	   			<?php echo $this->Html->link('Todos', array('controller'=>'analysis','action'=>'index'))?>
	  		</li>
	  	<?php endif; ?>
 	</ul>
</div>

	
	<label style="float:right;margin-top:6px;font-size:16px;font-weight:300;margin-right:10px;">Filtros:</label>

	<?php if($this->Session->read('Auth.User.level') > 3): ?>
	<?php echo $this->Form->create('Analysi', array('action' => 'pdfCoord')); ?>
	<?php endif; ?>
	<h2><?php echo 'Análises' ?></h2>
	<?php if($analysis == null && !$filter): echo 'Nehuma análise cadastrada'?>	
	<?php endif; ?>
	<?php if($analysis == null && $filter && $filterStatus): echo 'Nenhuma análise em ',$courses[$filter], ' com status ', $status[$filterStatus]; ?>
	<?php else: ?>	
		<table cellpadding="0" cellspacing="0" class="table table-hover">
		<tr>
			<?php if($this->Session->read('Auth.User.level') > 3): ?>
				<th><input type="checkbox" class="select_all" onClick="toggle(this);" /></th>
			<?php endif; ?>
			
			<th><?php echo 'Aluno' ?></th>
			<th><?php echo 'Curso' ?></th>
			<th><?php echo 'Status' ?></th>
			<th><?php echo 'Criado em' ?></th>
			<th><?php echo 'Ações' ?></th>
		</tr>
		<?php $cont = 0;?>
		<?php foreach ($analysis as $analysi): ?>
			<tr>
				<?php if($this->Session->read('Auth.User.level') > 3): ?>
					<td>
						<?php if($analysi['Analysi']['status'] == 3): ?>
							<?php echo $this->Form->input('name', array('type' => 'hidden', 'name'=>'data[Analysi]['.$cont.'][name]', 'value' => $analysi['User']['name']));?>
							<?php echo $this->Form->input('course', array('type' => 'hidden', 'name'=>'data[Analysi]['.$cont.'][course]', 'value' => $analysi['Course']['name']));?>
							<?php echo $this->Form->checkbox('registration', array('hiddenField' => false, 'name'=>'data[Analysi]['.$cont.'][registration]')); ?>
							<?php $cont ++;?>
						<?php else: ?>
							<input type="checkbox" class="checkbox" disabled />
							
						<?php endif; ?>
					</td>
				<?php endif; ?>
				
				<td><?php echo h($analysi['User']['name']); ?>&nbsp;</td>
				<td><?php echo h($analysi['Course']['name']); ?>&nbsp;</td>
				<td><?php echo h($status[$analysi['Analysi']['status']]); ?>&nbsp;</td>
				<td><?php echo $this->DateTime->formatDateTime($analysi['Analysi']['created']); ?>&nbsp;</td>
				<td class="actions">
					<?php if($analysi['Analysi']['status'] == 3 && $this->Session->read('Auth.User.level') == 3):?>
						<span class="btn btn-success btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Html->link('Gerar PDF', array('action'=>'viewPdf', $analysi['Analysi']['id']));?></span>
					<?php endif; ?>
						<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Html->link('Visualizar/Editar', array('controller' => 'activities', 'action' => 'show', $analysi['Analysi']['id'])); ?></span>
						<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Form->postLink('Deletar', array('action' => 'delete', $analysi['Analysi']['id']), array(), __('Are you sure you want to delete # %s?', $analysi['Analysi']['id'])); ?></span>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<p style="text-align:center;">
	<?php endif; ?>
	<?php if($this->Session->read('Auth.User.level') > 3): ?>
		<?php if($analysis != null) :?>
		<?php
			echo $this->Form->submit(
				'Gerar PDF', 
				array('class' => 'btn btn-success btn-block', 'style' => 'width:90px; text-align:center;')
			);	
		?>
		<?php echo $this->Form->end(); ?>
		<?php endif; ?>
	
	<?php endif; ?>
	</p>
</div>
