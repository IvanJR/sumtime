<?php 
App::import('Vendor','tcpdf/tcpdf'); 

$student = array();

//carrega o construtor do pdf
$Pdf = new TCPDF();

$Pdf->setPrintHeader(false);

$Pdf->setPrintFooter(false);

$Pdf->SetMargins(10, 20, 0);

$Pdf->AddPage('P','A4');

//debug($final); //$final eh o array com os nomes dos alunos q foram selecionados, agora eh soh montar o pdf

$html = "
<html>
	<head><title>Certificado de Horas Extra-Curriculares</title></head>
	<body style=\"width:100%;\">
	<div style=\"text-align:center;\">
		<img src=\"{$this->webroot}app/webroot/images/pdf/UFPEL-ESCUDO-2013.png\"  width=\"60\" height=\"60\">
	</div>

	<div>
		<hr style=\"width:96%; margin-left:0 auto; margin-right: 0 auto; margin-bottom:20px;\">
	</div>

	<div style=\"text-align:center;\">
		<b>MINISTÉRIO DA EDUCAÇÃO</b> <br>
		<b>UNIVERSIDADE FEDERAL DE PELOTAS</b> <br>
		<b>CENTRO DE DESENVOLVIMENTO TECNOLÓGICO</b> <br>	    
	</div>

	<p>A coordenadoria atesta para devidos fins que os docentes listados abaixo concluíram a carga horária
	mínima em atividades extra-curriculares conforme as normas atuais dos seus respectivos cursos: </p>
	
	<table style=\"padding:5px;width:95%; \">
		<tr style=\"background-color:#e5e5e5;\">
			<th style=\"border:1px solid #ccc;\">Nomes</th>
		</tr>
	";
	//debug($final);die;
for($i = 0; $i < count($final) ; $i++){
    $html .= "<tr>
		<td style=\"border:1px solid #ccc;\">{$final[$i]['name']}</td>
	</tr>
	";

}

$html .=   "</table>
		</div>
		<div style=\"text-align:center;\">
		<br/>
			________________________________ <br/>
				Coordenador(a) do Curso
		</div>
	</body>
</html>";


$Pdf->writeHTML($html);

echo $Pdf->Output('certificado.pdf','I'); 
