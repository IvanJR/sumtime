<?php
App::uses('Helper', 'View');

class DateTimeHelper extends AppHelper {
	
	public function formatDateTime($date) {
		if(!empty($date)){
			$temp = new DateTime($date);
			return $temp->format('d/m/Y H:i:s');
		} else {
			return '';
		}
	}
	
	public function formatDate($date) {
		if(!empty($date)){
			$temp = new DateTime($date);
			return $temp->format('d/m/Y');
		} else {
			return '';
		}
	}
}
