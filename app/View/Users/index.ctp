<?php echo $this -> Html -> link(__('Adicionar'), array('action' => 'admin_add'), array('class'=>'btn btn-small btn-primary')); ?>
<br><br>
<?php if (sizeof($users)==0) : echo 'Nenhum resultado foi encontrado.'; endif ?>
	
<?php if (sizeof($users)>0) : ?>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<th><?php echo $this->Paginator->sort('name', 'Nome'); ?></th>
		<th>Curso</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['name']); ?></td>
				<?php if(isset($user['User']['course_id'])):?>
					<td><?php echo h($courses[$user['User']['course_id']]); ?></td>
				<?php else: ?>
					<td><?php echo 'realizando cadastro'; ?></td>
				<?php endif; ?>
				<td class="actions">
					<?php echo $this->Html->link(__('Visualizar'), array('action' => 'view',$user['User']['id'])); ?>
					<?php echo $this->Html->link(__('Editar'), array('action' => 'admin_edit',$user['User']['id'])); ?>
					<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete',$user['User']['id']), null, __('Deseja excluir o registro #%s?', $user['User']['name'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->Element('admin/Layout/paginate');?>

<?php endif; ?>