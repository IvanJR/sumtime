<div class="alert alert-warning">
  <button type='button' class="close" data-dismiss="alert">
    <i>x</i>
  </button>
  <i class="icon-ok orange"></i>
  <?php echo $message ?>
</div>
