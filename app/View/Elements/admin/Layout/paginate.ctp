<?php echo $this->Paginator->counter('Número de registros: {:count}');?>
<div class="dataTables_paginate paging_bootstrap pagination">
	<ul>
		<?php echo $this -> Paginator -> prev('‹‹', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));?>
		<?php echo $this -> Paginator -> numbers(array(
										'separator' => '', 
										'class'=>'',
										'currentTag' => 'a',
										'currentClass' => 'active',
										'tag' => 'li')); ?>
		<?php echo $this -> Paginator -> next('››', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));?>
	</ul>
</div>
