<?php

class UploadComponent extends Component {

   public $config = array(
		'upload' => array(
			'photo' => array(
				'height' => 264,
				'width' => 469,
				'quality' =>100,
				'folder' => 'uploads/posts/',
				'allowed' => array(
					'jpg','jpeg','gif','png'
				)
			),
			'archive' => array(
				'folder' => 'uploads/conventions/',
				'allowed' => array(
					'pdf', 'xml'
				)
			),
			'video' => array(
				'folder' => 'uploads/videos/',
				'allowed' => array(
					'mp4','flv'
				)
			)
		),
		
		);
		
	public function sendImageFixed($file){
		$key = array_search(strtolower($this->getExt($file['name'])), $this->config['upload']['photo']['allowed']);
		if ($key===false){
			return false;
		} else {
			$folder = WWW_ROOT.$this->config['upload']['photo']['folder'];
			$new_file = uniqid().".".$this->getExt($file['name']);
			@mkdir($folder);		
			if (move_uploaded_file($file['tmp_name'], $folder . $new_file)){
				$this->resizeFixed($new_file);
				return $this->config['upload']['photo']['folder'].$new_file;
			} else return false;			
		}
	}
	
	public function sendArchive($file){
		$key = array_search(strtolower($this->getExt($file['name'])), $this->config['upload']['archive']['allowed']);
		if ($key===false){
			return false;
		} else {
			$folder = WWW_ROOT.$this->config['upload']['archive']['folder'];
			$new_file = uniqid().".".$this->getExt($file['name']);
			@mkdir($folder);		
			if (move_uploaded_file($file['tmp_name'], $folder . $new_file)){
				return $this->config['upload']['archive']['folder'].$new_file;
			} else return false;			
		}
	}
	
	public function sendVideo($file){
		$key = array_search(strtolower($this->getExt($file['name'])), $this->config['upload']['video']['allowed']);
		if ($key===false){
			return false;
		} else {
			$folder = WWW_ROOT.$this->config['upload']['video']['folder'];
			$new_file = uniqid().".".$this->getExt($file['name']);
			@mkdir($folder);		
			if (move_uploaded_file($file['tmp_name'], $folder . $new_file)){
				return $this->config['upload']['video']['folder'].$new_file;
			} else return false;			
		}
	}
	
	public function removeFile($file){
		@unlink(WWW_ROOT.$file);
	}
	
	private function getExt($file) {
		return pathinfo(WWW_ROOT.$this->config['upload']['photo']['folder'].$file, PATHINFO_EXTENSION);
	}
	
	private function resizeFixed($file){
		switch($this->getExt($file))
		{
		    case "png":
				$img = imagecreatefrompng(WWW_ROOT.$this->config['upload']['photo']['folder'].$file);
				$isTrueColor = imageistruecolor($img);
				$img_w = imagesx($img);
				$img_h = imagesy($img);
		        break;
		    case "gif":
				$img = imagecreatefromgif(WWW_ROOT.$this->config['upload']['photo']['folder'].$file);
				$img_w = imagesx($img);
				$img_h = imagesy($img);
				$new_file = imagecreatetruecolor($this->config['upload']['photo']['width'],$this->config['upload']['photo']['height']);
				imagecopyresized($new_file,$img,0,0,0,0,$this->config['upload']['photo']['width'],$this->config['upload']['photo']['height'],$img_w,$img_h);
		        break;
		    default:
				$img = imagecreatefromjpeg(WWW_ROOT.$this->config['upload']['photo']['folder'].$file);
				$img_w = imagesx($img);
				$img_h = imagesy($img);
				$new_file = imagecreatetruecolor($this->config['upload']['photo']['width'],$this->config['upload']['photo']['height']);
				imagecopyresized($new_file,$img,0,0,0,0,$this->config['upload']['photo']['width'],$this->config['upload']['photo']['height'],$img_w,$img_h);
				break;
		} 
		
		switch($this->getExt($file))
		{
		    case "png":
		    	$isTrueColor = imageistruecolor($img);
				if ($isTrueColor){
				    $new_file  = imagecreatetruecolor($this->config['upload']['photo']['width'],$this->config['upload']['photo']['height']);
				    imagealphablending($new_file, false);
				    imagesavealpha  ( $new_file  , true );
				} else {
				    $new_file  = imagecreate($this->config['upload']['photo']['width'],$this->config['upload']['photo']['height']);
				    imagealphablending( $new_file, false );
				    $transparent = imagecolorallocatealpha( $new_file, 0, 0, 0, 127 );
				    imagefill( $new_file, 0, 0, $transparent );
				    imagesavealpha( $new_file,true );
				    imagealphablending( $new_file, true );
				}
				imagecopyresampled($new_file, $img, 0, 0, 0, 0, $this->config['upload']['photo']['width'],$this->config['upload']['photo']['height'],$img_w,$img_h);
				imagepng($new_file,WWW_ROOT.$this->config['upload']['photo']['folder'].$file);
		        break;
		    case "gif":
				imagegif($new_file,WWW_ROOT.$this->config['upload']['photo']['folder'].$file,$this->config['upload']['photo']['quality']);
		        break;
		    default:
				imagejpeg($new_file,WWW_ROOT.$this->config['upload']['photo']['folder'].$file,$this->config['upload']['photo']['quality']);
				break;
		}

	}
}
